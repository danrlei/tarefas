<?php
class Dashboard_model extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function get_dashboard_solic_setor(){
		$this->db->select("distinct ds_setor, id_setor", false);
		$this->db->from("setor");
		$r['setores'] = $this->db->get()->result_array();

		foreach($r['setores'] as $i => $s){
			$this->db->select('count(1) as abertas');
			$this->db->from('solicitacao s');
			$this->db->join('tipo_tarefa tt', 'tt.id_tipo_tarefa = s.id_tipo_tarefa');
			$this->db->join('setor st', 'st.id_setor = tt.id_setor');
			$this->db->where('id_status <> 5');
			$this->db->where('st.id_setor', $r['setores'][$i]['id_setor']);
			$r['setores'][$i]['abertas'] = $this->db->get()->result_array()[0]['abertas'];
		}

		foreach($r['setores'] as $i => $s){
			$this->db->select('count(1) as fechadas');
			$this->db->from('solicitacao s');
			$this->db->join('tipo_tarefa tt', 'tt.id_tipo_tarefa = s.id_tipo_tarefa');
			$this->db->join('setor st', 'st.id_setor = tt.id_setor');
			$this->db->where('id_status  = 4');
			$this->db->where('st.id_setor', $r['setores'][$i]['id_setor']);
			$r['setores'][$i]['fechadas'] = $this->db->get()->result_array()[0]['fechadas'];
		}
		return $r;
	}
}