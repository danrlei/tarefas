<?php
if (!defined('BASEPATH')) {exit('No direct script access allowed');} 

class Dashboard extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->data['rodape'] = [];
        $this->data['rodape']['js'][] = modulo_js('dashboard.js');

    }

	public function index(){
        $this->data['interna'] = [];
        $this->data['carregar']['interna'] = 'dashboard';
        $this->templates->padrao($this->data);
	}

    public function get_dashboard_solic_setor(){
        echo json_encode($this->model->get_dashboard_solic_setor());
    }

}
