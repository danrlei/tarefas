
                        <div class="row ">
                            <div class="col-xl-12 col-md-6">
                                <!-- Begin Widget 09 -->
                                <div class="widget widget-09 has-shadow">
                                    <!-- Begin Widget Header -->
                                    <div class="widget-header d-flex align-items-center">
                                        <h3>Solicitações por Setor</h3>
                                    </div>
                                    <!-- End Widget Header -->
                                    <!-- Begin Widget Body -->
                                    <div class="widget-body">
                                        <div class="row">
                                            <div class="col-xl-10 col-12 no-padding">
                                                <div>
                                                    <canvas id="orders"></canvas>
                                                </div>
                                            </div>
                                            <div class="col-xl-2 col-12 d-flex flex-column my-auto no-padding text-center">
                                                <div class="some-stats mt-5">
                                                    <div class="title">Abertas</div>
                                                    <div class="number text-blue" id="abertas"></div>
                                                </div>
                                                <div class="some-stats mt-3">
                                                    <div class="title">Fechadas</div>
                                                    <div class="number text-blue" id="fechadas"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Widget 09 -->
                            </div>
                        </div>