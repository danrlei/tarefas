<?php
class Minhas_tarefas_model extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function assumir($idSolicitacao){
		$this->db->set('id_executante', $this->session->userdata['tarefas']['id_funcionario']);
		$this->db->where('id_solicitacao', $idSolicitacao);
		return $this->db->update('solicitacao');
	}

	public function assumir_tarefa($idTarefa){
		$this->db->set('id_executante', $this->session->userdata['tarefas']['id_funcionario']);
		$this->db->where('id_solicitacao_tarefa', $idTarefa);
		return $this->db->update('solicitacao_tarefa');
	}

	public function finalizar_implementacao($idSolicitacao){ 
		if(count($this->model->get_tarefas_solicitacao($idSolicitacao)) > 0){
			if($this->get_qtd_aprovacao($idSolicitacao) > 0){
				$idStatus = 2;
			} else {
				$idStatus = 3;
			}
			$this->db->set('id_executante', 'null', false);
			$this->db->set('id_status', $idStatus);
			$this->db->where('id_solicitacao', $idSolicitacao);
			$this->db->update('solicitacao');

			$this->db->set('id_status', $idStatus);
			$this->db->where('id_solicitacao', $idSolicitacao);
			return $this->db->update('solicitacao_tarefa');
		} else {
			return 'solicitacao_sem_acoes';
		}
	}

	public function aprovar($idSolicitacao){
		$idAprovacao = $this->get_id_aprovacao($idSolicitacao);
		$qtdAprovacao = $this->get_qtd_aprovacao($idSolicitacao);
		$aprovacaoAtual = $this->get_qtd_aprovacao_fechadas($idSolicitacao) + 1;

		if($qtdAprovacao == $aprovacaoAtual){
			$this->db->set('id_executante', 'null', false);
			$this->db->set('id_status', 3);
			$this->db->where('id_solicitacao', $idSolicitacao);
			$this->db->update('solicitacao');

			$this->db->set('id_status', 3);
			$this->db->where('id_solicitacao', $idSolicitacao);
			$this->db->update('solicitacao_tarefa');	
		}

		$solicitacao_aprovacao = array(
			'id_aprovacao' => $idAprovacao,
			'id_aprovador' => $this->session->userdata['tarefas']['id_funcionario'],
			'id_solicitacao' => $idSolicitacao
		);

		return $this->db->insert('solicitacao_aprovacao', $solicitacao_aprovacao);
	}

	public function reprovar($idSolicitacao){
		$this->db->set('id_executante', 'null', false);
		$this->db->set('id_status', 5);
		$this->db->where('id_solicitacao', $idSolicitacao);
		$this->db->update('solicitacao');

		$this->db->set('id_status', 5);
		$this->db->where('id_solicitacao', $idSolicitacao);
		return $this->db->update('solicitacao_tarefa');
	}

	public function get_tarefas_solicitacao($idSolicitacao){
        $this->db->select('*');
        $this->db->from('solicitacao_tarefa');
        $this->db->where('id_solicitacao', $idSolicitacao);
		return $this->db->get()->result_array();
    }

	public function insert_solicitacao_tarefa($dados){
		return $this->db->insert('solicitacao_tarefa',$dados);
	}

	public function insert_solicitacao_tarefa_func($dados){
		return $this->db->insert('solicitacao_tarefa_func',$dados);
	}

	public function insert_solicitacao_tarefa_grupo($dados){
		return $this->db->insert('solicitacao_tarefa_grupo',$dados);
	}

	public function insert_solicitacao_log($dados){
		return $this->db->insert('solicitacao_log', $dados);
	}

	public function insert_solicitacao_tarefa_anexo($dados){
		return $this->db->insert('solicitacao_tarefa_anexo', $dados);
	}

	public function insert_solicitacao_tarefa_log($dados){
		return $this->db->insert('solicitacao_tarefa_log', $dados);
	}

	public function get_qtd_aprovacao($idSolicitacao){
		$this->db->select('count(a.id_aprovacao) as possui_aprovacao');
		$this->db->from('solicitacao s');
		$this->db->join('tipo_tarefa tt','s.id_tipo_tarefa = tt.id_tipo_tarefa','left');
		$this->db->join('aprovacao a','tt.id_tipo_tarefa = a.id_tipo_tarefa','left');
        $this->db->where('s.id_solicitacao', $idSolicitacao);

        return $this->db->get()->result()[0]->possui_aprovacao;
	}

	public function get_qtd_aprovacao_fechadas($idSolicitacao){
		$this->db->select('count(a.id_aprovacao) as possui_aprovacao');
		$this->db->from('solicitacao s');
		$this->db->join('tipo_tarefa tt','s.id_tipo_tarefa = tt.id_tipo_tarefa','left');
		$this->db->join('aprovacao a','tt.id_tipo_tarefa = a.id_tipo_tarefa','left');
		$this->db->join('solicitacao_aprovacao sa','a.id_aprovacao = sa.id_aprovacao and s.id_solicitacao = sa.id_solicitacao');
        $this->db->where('s.id_solicitacao', $idSolicitacao);

        return $this->db->get()->result()[0]->possui_aprovacao;
	}

	public function get_id_aprovacao($idSolicitacao){
		$query = "";
		$query .=' select min(id_aprovacao) id_aprovacao from ( ';
		$query .=' 	select a.id_aprovacao ';
		$query .=' 	from solicitacao s ';
		$query .=' 	join tipo_tarefa t on s.id_tipo_tarefa = t.id_tipo_tarefa ';
		$query .=' 	join aprovacao a on t.id_tipo_tarefa = a.id_tipo_tarefa ';
		$query .=' 	join aprovacao_grupo ag on a.id_aprovacao = ag.id_aprovacao ';
		$query .=' 	join grupo g on g.id_grupo = ag.id_grupo ';
		$query .=' 	join funcionario_grupo fg on g.id_grupo = fg.id_grupo ';
		$query .=' 	left join solicitacao_aprovacao sa on a.id_aprovacao = sa.id_aprovacao and sa.id_solicitacao = s.id_solicitacao ';
		$query .=' 	where s.id_solicitacao = '.$idSolicitacao.' ';
		$query .=' 	and fg.id_funcionario = '.$this->session->userdata['tarefas']['id_funcionario'].' ';
		$query .=' 	and sa.id_solicitacao_aprovacao is null ';
		$query .=' 	union ';
		$query .=' 	select a2.id_aprovacao ';
		$query .=' 	from solicitacao s2 ';
		$query .=' 	join tipo_tarefa t2 on s2.id_tipo_tarefa = t2.id_tipo_tarefa ';
		$query .=' 	join aprovacao a2 on t2.id_tipo_tarefa = a2.id_tipo_tarefa ';
		$query .=' 	join aprovacao_funcionario af2 on af2.id_aprovacao = a2.id_aprovacao ';
		$query .=' 	left join solicitacao_aprovacao sa2 on a2.id_aprovacao = sa2.id_aprovacao and sa2.id_solicitacao = s2.id_solicitacao ';
		$query .=' 	where s2.id_solicitacao = '.$idSolicitacao.' ';
		$query .=' 	and af2.id_funcionario = '.$this->session->userdata['tarefas']['id_funcionario'].' ';
		$query .=' 	and sa2.id_solicitacao_aprovacao is null ';
		$query .=' ) t ';
		$this->db->query($query);

		return $this->db->query($query)->result()[0]->id_aprovacao;
	}

	public function get_tarefas(){
		$this->db->select("
			case 
				when st.id_executante is null  then 'assumir'
				when s.id_status = 3 then 'finalizar'
			end as acao,
			st.id_solicitacao_tarefa, 
			st.ds_solicitacao_tarefa, 
			s.ds_status, 
			st.id_solicitacao,
			to_char(st.dt_criacao, 'DD/MM/YYYY HH24:MI') dt_criacao,
			sol.titulo,
			regexp_replace(sol.ds_solicitacao, E'\r\n|\n|\r', '<br />', 'g') ds_solicitacao, 
			to_char(sol.dt_criacao, 'DD/MM/YYYY HH24:MI') dt_criacao_sol,	
			to_char(sol.dt_prazo, 'DD/MM/YYYY') dt_prazo_sol,				
			setor.ds_setor,
			tt.ds_tipo_tarefa,
			f.nm_funcionario,
			to_char(st.dt_prazo, 'DD/MM/YYYY') dt_prazo
		");
		$this->db->from("solicitacao_tarefa st");
		$this->db->join("solicitacao_tarefa p", "p.id_solicitacao_tarefa = st.id_tarefa_pendencia", "left");
		$this->db->join("status s", "st.id_status = s.id_status");
		$this->db->join("solicitacao sol", "st.id_solicitacao = sol.id_solicitacao");
		$this->db->join('tipo_tarefa tt', 'sol.id_tipo_tarefa = tt.id_tipo_tarefa');
		$this->db->join('setor', 'setor.id_setor = tt.id_setor');
		$this->db->join('funcionario f', 'f.id_funcionario = sol.id_solicitador');
		$this->db->where("st.id_status", 3);
		$this->db->where($this->session->userdata['tarefas']['id_funcionario']." IN(
			select fg2.id_funcionario
			from solicitacao_tarefa_grupo stg2
			join grupo g2 on stg2.id_grupo = g2.id_grupo
			join funcionario_grupo fg2 on fg2.id_grupo = g2.id_grupo
			where st.id_solicitacao_tarefa = stg2.id_solicitacao_tarefa
			union 
			select stf2.id_funcionario
			from solicitacao_tarefa_func stf2
			where st.id_solicitacao_tarefa = stf2.id_solicitacao_tarefa
		)");
		$this->db->where("(st.id_tarefa_pendencia is null or p.id_status = 4)");
		$this->db->order_by('st.dt_criacao desc');
		$retorno = $this->db->get()->result_array();

		foreach($retorno as $i => $r){
			$this->db->select("distinct st.id_solicitacao_tarefa, ds_solicitacao_tarefa, to_char(st.dt_criacao, 'DD/MM/YYYY') as criacao, STRING_AGG(distinct f.nm_funcionario, ', ') as funcionarios, STRING_AGG(distinct g.ds_grupo, ', ') as grupos, id_tarefa_pendencia, sta.ds_status, to_char(st.dt_prazo, 'DD/MM/YYYY') dt_prazo", false);
			$this->db->from('solicitacao_tarefa st');
			$this->db->join('solicitacao_tarefa_func stf','st.id_solicitacao_tarefa = stf.id_solicitacao_tarefa','left');
			$this->db->join('solicitacao_tarefa_grupo stg','st.id_solicitacao_tarefa = stg.id_solicitacao_tarefa','left');
			$this->db->join('funcionario f','stf.id_funcionario = f.id_funcionario','left');
			$this->db->join('grupo g','stg.id_grupo = g.id_grupo','left');
			$this->db->join('status sta', 'st.id_status = sta.id_status');
			$this->db->where('id_solicitacao', $r['id_solicitacao']);
			$this->db->group_by('st.id_solicitacao_tarefa, sta.ds_status');
			$this->db->order_by('st.id_solicitacao_tarefa');

			$retorno[$i]['tarefas'] = $this->db->get()->result_array();

			$this->db->select('id_solicitacao_tarefa, log, to_char(dt_criacao, \'DD/MM/YYYY HH24:MI:SS\') as criacao');
			$this->db->from('solicitacao_tarefa_log');
			$this->db->where('id_solicitacao_tarefa', $r['id_solicitacao_tarefa']);
			$this->db->order_by('dt_criacao desc');

			$retorno[$i]['logs'] = $this->db->get()->result_array();

		}

		return $retorno;
	}

	public function finalizar($p){
		$this->db->set('observacao_execucao', $p['observacao_execucao']);
		$this->db->set('id_status', 4);
		$this->db->where('id_solicitacao_tarefa', $p['id_solicitacao_tarefa']);
		$this->db->update('solicitacao_tarefa');

		$query = "";
		$query .= " select count(1) as qtd_abertos ";
		$query .= " from solicitacao_tarefa st ";
		$query .= " join solicitacao_tarefa st2 on st.id_solicitacao = st2.id_solicitacao ";
		$query .= " where st.id_solicitacao_tarefa = ".$p['id_solicitacao_tarefa']." ";
		$query .= " and st2.id_status <> 4 ";
		$this->db->query($query);
		if($this->db->query($query)->result()[0]->qtd_abertos == 0){
			$query = "";
			$query .= " select id_solicitacao ";
			$query .= " from solicitacao_tarefa ";
			$query .= " where id_solicitacao_tarefa = ".$p['id_solicitacao_tarefa']." ";

			$this->db->query($query);
			$idSolicitacao = $this->db->query($query)->result()[0]->id_solicitacao;
		
			$this->db->set('id_status', 4);
			$this->db->where('id_solicitacao', $idSolicitacao);
			$this->db->update('solicitacao');
		}
	}

	public function get_detalhes_tarefa($idTarefa){
		$this->db->select("*");
		$this->db->from("solicitacao_tarefa");
		$this->db->where("id_solicitacao_tarefa", $idTarefa);
		$retorno = $this->db->get()->result_array();

		$this->db->select("*");
		$this->db->from("solicitacao_tarefa_anexo");
		$this->db->where("id_solicitacao_tarefa", $idTarefa);
		$retorno['arquivos'] = $this->db->get()->result_array();

		return $retorno;
	}
}