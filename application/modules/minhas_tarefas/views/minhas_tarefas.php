
                <div class="widget widget-21 has-shadow">
                    <div class="widget-header bordered no-actions d-flex align-items-center ">
                        <h2>Minhas Tarefas</h2>
                    </div>
                    <div class="widget-body m--padding-top-20  m--padding-bottom-0">

						<div class="col-lg-12  col-lg-mobile  m--padding-bottom-15 m--padding-top-10">
                            <h2 style="margin-bottom: 20px;"> Solicitações</h2>  
                            <div id="accordion" class="accordion">
                                <!-- Begin Widget -->
                                <div class="row titulo-card">
                                    <div class="col-1">ID</div>
                                    <div class="col-4">Título</div>
                                    <div class="col-3">Status</div>
                                    <div class="col-1">Concluído</div>
                                    <div class="col-1">Prazo</div>
                                    <div class="col-2">
                                    </div>
                                </div>
                                <div class="widget has-shadow" id="solicitacoes">

                                </div>
                                <!-- End Widget -->
                            </div>
						</div>

                        <div class="col-lg-12  col-lg-mobile  m--padding-bottom-15 m--padding-top-10">
                            <h2 style="margin-bottom: 20px;"> Tarefas</h2>  
                            <div id="accordion2" class="accordion">
                                <!-- Begin Widget -->
                                <div class="row titulo-card">
                                    <div class="col-1">ID</div>
                                    <div class="col-3">Descrição</div>
                                    <div class="col-3">Status</div>
                                    <div class="col-2">Criação</div>
                                    <div class="col-1">Prazo</div>
                                    <div class="col-2">
                                    </div>
                                </div>
                                <div class="widget has-shadow" id="tarefas">

                                </div>
                                <!-- End Widget -->
                            </div>
                        </div>
					</div>
				</div>


<div id="modal-implementar" class="modal fade">
    <div class="modal-dialog modal-dialog-centered  modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <h4 class="modal-title">Implementar Solicitação</h4>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">close</span>
                </button>
            </div>
            <form class="form-horizontal align-items-center" id="form-implementar" method="post" action="<?php echo base_url('minhas_tarefas/implementar')?>">
                <input type="hidden" name="id_solicitacao" id="id_solicitacao">
                <div class="modal-body">
                    <div class="form-group row d-flex align-items-center ">
                        <div class="col-md-12 m--margin-bottom-5">
                            <label class="form-control-label">O que deseja fazer?</label>
                            <select id="acao" name="acao" class="custom-select form-control acao" required>
                                <option value="incluir">Incluir Tarefa</option>
                                <option value="finalizar">Finalizar Implementação</option>
                                <option value="reprovar">Reprovar Solicitação</option>
                            </select>                   
                        </div>
                    </div>
                    <div id="form-reprovar-solicitacao">
                        <div class="form-group row align-items-center" id="motivo">
                            <div class="col-md-12 m--margin-bottom-10">
                                <label class="form-control-label">Motivo</label>
                                <textarea class="form-control" rows="5" id="txt-motivo" name="motivo"></textarea>
                            </div>
                        </div>
                    </div>
                    <div id="form-incluir-acao">
                        <div class="form-group row d-flex align-items-center ">
                            <div class="col-md-9 m--margin-bottom-5">
                                <label class="form-control-label">Descrição</label>
                                <input type="text" class="form-control" maxlength="100" id="ds_solicitacao_tarefa" name="ds_solicitacao_tarefa">
                            </div>
                            <div class="col-md-3 m--margin-bottom-5">
                                <label class="form-control-label">Prazo</label>
                                <input type="text" class="form-control datepicker" name="dt_prazo" id="dt_prazo">
                            </div>                                            
                        </div>
                        <div class="form-group row d-flex align-items-center ">
                            <div class="col-md-6 m--margin-bottom-5">
                                <label class="form-control-label">Grupos</label>
                                <select class="form-control selectpicker" multiple="multiple" name="grupos[]">
                                    <?php foreach($grupos as $g){ ?>
                                        <option value="<?php echo $g['id_grupo'] ?>"><?php echo $g['ds_grupo'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-6 m--margin-bottom-5">
                                <label class="form-control-label">Funcionários</label>
                                <select class="form-control selectpicker" multiple="multiple" name="funcionarios[]">
                                    <?php foreach($funcionarios as $f){ ?>
                                        <option value="<?php echo $f['id_funcionario'] ?>"><?php echo $f['nm_funcionario'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-12 form-control-label">Pendência</label>
                            <div class="d-pendencias col-sm-12" style="padding-left: 15px;">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-shadow btn-cancelar" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="modal-aprovar" class="modal fade">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content ">
            <div class="modal-header">
                <h4 class="modal-title">Aprovação</h4>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">close</span>
                </button>
            </div>
            <form class="form-horizontal align-items-center" id="form-aprovacao" method="post" action="<?php echo base_url('minhas_tarefas/aprovacao')?>">
                <input type="hidden" name="id_solicitacao" id="id_solicitacao">
                <div class="modal-body">
                    <div class="form-group row d-flex align-items-center ">
                        <div class="col-md-12 m--margin-bottom-5">
                            <label class="form-control-label">O que deseja fazer?</label>
                            <select id="acao" name="acao" class="custom-select form-control acao" required>
                                <option value="aprovar">Aprovar</option>
                                <option value="reprovar">Reprovar</option>
                            </select>                   
                        </div>
                    </div>
                    <div class="form-group row align-items-center" id="d-motivo">
                        <div class="col-md-12 m--margin-bottom-10">
                            <label class="form-control-label">Motivo</label>
                            <textarea id="txt-motivo-rep" class="form-control" rows="5" name="motivo"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-shadow btn-cancelar" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="modal-finalizar" class="modal fade">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content ">
            <div class="modal-header">
                <h4 class="modal-title">Finalizar</h4>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">close</span>
                </button>
            </div>
            <form  enctype="multipart/form-data" class="form-horizontal align-items-center" id="form-finalizar" method="post" action="<?php echo base_url('minhas_tarefas/finalizar')?>">
                <input type="hidden" name="id_solicitacao_tarefa" id="id_solicitacao_tarefa">
                <div class="modal-body">
                    <div class="form-group row d-flex align-items-center ">
                        <div class="col-md-12 m--margin-bottom-5">
                            <label class="form-control-label">Observação</label>
                            <textarea class="form-control" rows="5" required name="observacao_execucao"></textarea>                   
                        </div>
                    </div>
                    <div class="form-group row align-items-center" id="motivo">
                        <div class="col-md-12 m--margin-bottom-10"> 
                            <label class="form-control-labe">Arquivos</label>
                            <input type="file" id="arquivos" name="arquivos[]" multiple class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-shadow btn-cancelar" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>