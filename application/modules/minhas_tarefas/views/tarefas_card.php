<?php foreach($tarefas as $i => $t){ ?>


<a class="card-header d-flex align-items-center collapsed" data-toggle="collapse" href="#tarefa-<?php echo $t['id_solicitacao_tarefa']; ?>" aria-expanded="false">
    <div class="card-title " style="font-size: 1.2rem; width: 100%;">
    	<div class="row">
    		<div class="col-1"><?php echo $t['id_solicitacao_tarefa']; ?></div>
    		<div class="col-3"><?php echo $t['ds_solicitacao_tarefa']; ?></div>
    		<div class="col-3"><?php echo $t['ds_status']; ?></div>
            <div class="col-2"><?php echo $t['dt_criacao']; ?></div>
            <div class="col-1"><?php echo $t['dt_prazo']; ?></div>
    		<div class="col-2">
                <?php if($t['acao'] == 'assumir'){ ?>
                    <button style="margin-top: -9px;" type="button" class="btn btn-shadow btn-novo"onClick="assumir_tarefa(<?php echo $t['id_solicitacao_tarefa']; ?>);">Assumir</button>
                <?php } else {?>
                    <button style="margin-top: -9px;" type="button" class="btn btn-shadow btn-novo" data-toggle="modal" data-target="#modal-finalizar" onClick="finalizar(<?php echo $t['id_solicitacao_tarefa']; ?>);">Finalizar</button>
                <?php } ?>
            </div>
    	</div>
    </div>
</a>
<div id="tarefa-<?php echo $t['id_solicitacao_tarefa']; ?>" class="card-body pt-0 collapse row" data-parent="#accordion2" style="font-size: 1.2rem;">
    <div class="col-12 m--margin-bottom-10">
        <h3>Solicitação</h3><br/>
        <div class="row">
            <div class="col-2 m--margin-bottom-10">
                <b>ID</b><br/>
                <?php echo $t['id_solicitacao']; ?>
            </div>
            <div class="col-10 m--margin-bottom-10">
                <b>Título</b><br/>
                <?php echo $t['titulo']; ?>
            </div>
            <div class="col-3 m--margin-bottom-10">
                <b>Setor</b><br/>
                <?php echo $t['ds_setor']; ?>
            </div>
            <div class="col-3 m--margin-bottom-10">
                <b>Tipo de Tarefa</b><br/>
                <?php echo $t['ds_tipo_tarefa']; ?>
            </div>
            <div class="col-2 m--margin-bottom-10">
                <b>Criação</b><br/>
                <?php echo $t['dt_criacao_sol']; ?>
            </div>
            <div class="col-2 m--margin-bottom-10">
                <b>Solicitador</b><br/>
                <?php echo $t['nm_funcionario']; ?>
            </div>
            <div class="col-2 m--margin-bottom-10">
                <b>Prazo</b><br/>
                <?php echo $t['dt_prazo_sol']; ?>
            </div>
            <div class="col-12 m--margin-bottom-10">
                <b>Descrição</b><br/>
                <?php echo $t['ds_solicitacao']; ?>
            </div>
        </div>
    </div>    
    <div class="col-12 m--margin-bottom-10">
        <h3>Tarefas </h3>
    </div>
    <div class="col-12 m--margin-bottom-30">
        <div class="table-responsive">
            <table class="table mb-0">
                <thead>
                    <tr>
                        <th style="width: 5%">ID</th>
                        <th>Descrição</th>
                        <th>Criação</th>
                        <th>Funcionários</th>
                        <th>Grupos</th>
                        <th>Pendência</th>
                        <th>Status</th>
                        <th>Prazo</th>
                        <th style="width: 5%"></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($t['tarefas'] as $i2 => $l){ ?>
                    <tr>
                        <td><?php echo $l['id_solicitacao_tarefa'] ?></td>
                        <td><?php echo $l['ds_solicitacao_tarefa'] ?></td>
                        <td><?php echo $l['criacao'] ?></td>
                        <td><?php echo $l['funcionarios'] ?></td>
                        <td><?php echo $l['grupos'] ?></td>
                        <td><?php echo $l['id_tarefa_pendencia'] ?></td>
                        <td><?php echo $l['ds_status'] ?></td>
                        <td><?php echo $l['dt_prazo'] ?></td>
                        <td style="width: 5%; text-align: center;" onclick="getDetalhesTarefa(<?php echo $l['id_solicitacao_tarefa'] ?>)" class="td-actions">
                            <a href="#" data-toggle="modal" data-target="#modal-detalhes-tarefa">
                                <i class="la la-search"></i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-12 m--margin-bottom-10">
        <h3>Histórico </h3>
    </div>
    <div class="col-12 m--margin-bottom-10">
        <div class="table-responsive">
            <table class="table mb-0">
                <thead>
                    <tr>
                        <th style="width: 70%">Descrição</th>
                        <th style="width: 20%">Data</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($t['logs'] as $i2 => $l){ ?>
                    <tr>
                        <td><?php echo $l['log'] ?></td>
                        <td><?php echo $l['criacao'] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php } ?>