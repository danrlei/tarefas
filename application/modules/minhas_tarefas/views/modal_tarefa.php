<div class="form-group row d-flex align-items-center ">	
    <div class="col-md-12 m--margin-bottom-10">
        <label class="form-control-label">Observações</label>
        <textarea class="form-control" value="" rows="5" readonly required name="observacao"><?php echo $tarefa[0]['observacao_execucao']; ?></textarea>
    </div>
    <div class="col-md-12 m--margin-bottom-10">
    	<label class="form-control-label">Anexos</label>
		<div class="form-group lista-arquivos">
			<?php foreach($tarefa['arquivos'] as $i => $a){ ?>
	        <a type="button" href="http://localhost/tarefas/application/uploads/tarefas/<?php echo $a['id_solicitacao_tarefa']; ?>/<?php echo $a['ds_arquivo']; ?>" download target="_blank" class="btn btn-primary mr-1 mb-2"><?php echo($a['ds_arquivo']); ?><i class="la la-download"></i></a>
	    	<?php } ?>
	    </div>
    </div>

</div>