$(document).ready(function() {
    getSolicitacoes();
    getTarefas();
});

$(document).on('change', '#setores', function(){
	setor = $(this).val();
	$.ajax({
        url: base_url + "setores/get_tipo_tarefa_by_setor/"+setor,
        type: 'get',
        dataType: 'json',
        success:function (data) {
        	option = "";
        	data.forEach(function(tp){
        		option += "<option value="+tp.id_tipo_tarefa+">"+tp.ds_tipo_tarefa+"</option>";
        	});
        	$('#tipo_tarefa').html(option);
        }
    }); 
})

$(document).on('change', '#form-implementar .acao', function(){
    implementar_mudar_acao();
})

$(document).on('change', '#form-aprovacao .acao', function(){
    aprovacao_mudar_acao();
})


function getSolicitacoes(){
    $.ajax({
        url: base_url + "minhas_solicitacoes/get_vw_solicitacao_card/minhas_tarefas",
        type: 'get',
        dataType: 'html',
        success:function (data) {
            $('#solicitacoes').html(data);
        }
    });  
}

function assumir(idSolicitacao){
    Swal.fire({
        title: 'Atenção',
        text: 'Deseja assumir a tarefa selecionada?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim!',
        cancelButtonText: 'Cancelar',
    }).then((result) => {
        if (result.value) {
            window.location.href = base_url + 'minhas_tarefas/assumir/' + idSolicitacao;
        }
    });
}

function assumir_tarefa(idTarefa){
    Swal.fire({
        title: 'Atenção',
        text: 'Deseja assumir a tarefa selecionada?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim!',
        cancelButtonText: 'Cancelar',
    }).then((result) => {
        if (result.value) {
            window.location.href = base_url + 'minhas_tarefas/assumir_tarefa/' + idTarefa;
        }
    });  
}

function implementar(idSolicitacao){
    resetar_form('form-implementar');
    $('#form-implementar #id_solicitacao').val(idSolicitacao);
    implementar_mudar_acao();
    buscar_tarefas_pendencia(idSolicitacao);
}

function implementar_mudar_acao(){
    $('#form-incluir-acao').css('display', 'none');
    $('#form-reprovar-solicitacao').css('display', 'none');
    $('#txt-motivo').removeAttr('required');
    $('#ds_solicitacao_tarefa').removeAttr('required');
    if($('#form-implementar .acao').val() == 'incluir'){
        $('#form-incluir-acao').css('display', 'block');
        $('#ds_solicitacao_tarefa').attr('required', 'required');
    } else if($('#form-implementar .acao').val() == 'reprovar'){
        $('#form-reprovar-solicitacao').css('display', 'block');
        $('#txt-motivo').attr('required', 'required');
    }
}

function aprovacao_mudar_acao(){
    if($('#form-aprovacao .acao').val() == 'reprovar'){
        $('#d-motivo').css('display', 'block');
        $('#txt-motivo-rep').attr('required', 'required');
    } else {
        $('#d-motivo').css('display', 'none');
        $('#txt-motivo-rep').removeAttr('required');
    }
}

function buscar_tarefas_pendencia(idSolicitacao){
    $.ajax({
        url: base_url + "minhas_tarefas/get_tarefas_solicitacao/"+idSolicitacao,
        type: 'get',
        dataType: 'json',
        success:function (data) {
            html = "";
            data.forEach(function(tarefas){

                html += '<div class="mb-3">';
                html += '   <div class="styled-radio">';
                html += '       <input type="radio" name="id_tarefa_pendencia" id="id_tarefa_pendencia-'+tarefas.id_solicitacao_tarefa+'" value="'+tarefas.id_solicitacao_tarefa+'">';
                html += '       <label for="id_tarefa_pendencia-'+tarefas.id_solicitacao_tarefa+'">'+tarefas.ds_solicitacao_tarefa+'</label>';
                html += '   </div>';
                html += '</div>';
            });
            console.log(html);
            $('.d-pendencias').html(html);
        }
    });  
}

function aprovar(idSolicitacao){
    resetar_form('form-aprovacao');
    $('#form-aprovacao #id_solicitacao').val(idSolicitacao);
    aprovacao_mudar_acao();
}

function getTarefas(){
    $.ajax({
        url: base_url + "minhas_tarefas/get_vw_tarefas_card",
        type: 'get',
        dataType: 'html',
        success:function (data) {
            $('#tarefas').html(data);
        }
    });  
}

function finalizar(idTarefa){
    resetar_form('form-finalizar');
    $('#form-finalizar #id_solicitacao_tarefa').val(idTarefa);
}

function getDetalhesTarefa(idTarefa){
    $.ajax({
        url: base_url + "minhas_tarefas/get_vw_modal_tarefa/"+idTarefa,
        type: 'get',
        dataType: 'html',
        success:function (data) {
            $('#modal-detalhes-tarefa .modal-body').html(data);
        }
    });  
}