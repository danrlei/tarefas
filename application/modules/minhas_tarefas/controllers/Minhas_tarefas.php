<?php
if (!defined('BASEPATH')) {exit('No direct script access allowed');} 

class Minhas_tarefas extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->data['rodape'] = [];
        $this->data['rodape']['js'][] = modulo_js('minhas_tarefas.js');

        $this->load->model('grupos/grupos_model','grupos');
        $this->load->model('funcionarios/funcionarios_model','funcionarios');
    }
	public function index(){
        $this->data['interna'] = [];
        $this->data['carregar']['interna'] = 'minhas_tarefas';
        $this->data['interna']['grupos'] = $this->grupos->get_all();
        $this->data['interna']['funcionarios'] = $this->funcionarios->get_all();
        $this->templates->padrao($this->data);        
	}

    public function assumir($idSolicitacao){
        $status = $this->model->assumir($idSolicitacao);
        if($status === false){
            $swal['titulo'] = 'Erro!';
            $swal['mensagem'] = 'Não foi possível executar a operação!';
            $swal['tipo'] = 'error';
            $this->session->set_flashdata('mensagem_swal', $swal);  
            redirect(base_url('minhas_tarefas'));
        }
        $log = array(
            'id_solicitacao' => $idSolicitacao,
            'log' => 'Solicitação assumida pelo funcionário '.$this->session->userdata['tarefas']['nm_funcionario'].'.'
        );
        $this->model->insert_solicitacao_log($log);

        $swal['titulo'] = 'Sucesso!';
        $swal['mensagem'] = 'Ação realizada com sucesso!';
        $swal['tipo'] = 'success';
        $this->session->set_flashdata('mensagem_swal', $swal); 
        redirect(base_url('minhas_tarefas'));
    }

    public function get_tarefas_solicitacao($idSolicitacao){
        echo json_encode($this->model->get_tarefas_solicitacao($idSolicitacao));
    }

    public function implementar(){
        $p = $this->input->post();

        if($p['acao'] == 'incluir'){
            $status = $this->incluir_tarefa($p);
        } else if($p['acao'] == 'finalizar') {
            $status = $this->finalizar_implementacao($p);
        } else if($p['acao'] == 'reprovar') {
            $status = $this->reprovar($p);
        }

        if($status === false){
            $swal['titulo'] = 'Erro!';
            $swal['mensagem'] = 'Não foi possível executar a operação!';
            $swal['tipo'] = 'error';
            $this->session->set_flashdata('mensagem_swal', $swal);  
            redirect(base_url('minhas_tarefas'));
        }

        if($status != 'solicitacao_sem_acoes'){
            $swal['titulo'] = 'Sucesso!';
            $swal['mensagem'] = 'Ação realizada com sucesso!';
            $swal['tipo'] = 'success';
            $this->session->set_flashdata('mensagem_swal', $swal); 
        }
        redirect(base_url('minhas_tarefas'));
    }

    public function incluir_tarefa($p){
        $grupos = isset($p['grupos']) ? $p['grupos'] : [];
        $funcionarios = isset($p['funcionarios']) ? $p['funcionarios'] : [];
        $solicitacao_tarefa = array(
            'id_solicitacao' => $p['id_solicitacao'],
            'id_status' => 1
        );

        if(isset($p['dt_prazo']) && $p['dt_prazo'] != ''){
            $solicitacao_tarefa['dt_prazo'] = date_pt_bd($p['dt_prazo']);
        }

        if(isset($p['id_tarefa_pendencia']) && $p['id_tarefa_pendencia'] != ''){
            $solicitacao_tarefa['id_tarefa_pendencia'] = $p['id_tarefa_pendencia'];
        } else {
            $solicitacao_tarefa['id_tarefa_pendencia'] = null;
        }

        if(isset($p['ds_solicitacao_tarefa']) && $p['ds_solicitacao_tarefa'] != ''){
            $solicitacao_tarefa['ds_solicitacao_tarefa'] = $p['ds_solicitacao_tarefa'];
        } else {
            $solicitacao_tarefa['ds_solicitacao_tarefa'] = null;
        }

        $this->model->insert_solicitacao_tarefa($solicitacao_tarefa);
        $id_solicitacao_tarefa = $this->db->insert_id();

        foreach($grupos as $id_grupo){
            $grupo = array(
                'id_solicitacao_tarefa' => $id_solicitacao_tarefa,
                'id_grupo' => $id_grupo
            );

            $this->model->insert_solicitacao_tarefa_grupo($grupo);
        }

        foreach($funcionarios as $id_funcionario){
            $funcionario = array(
                'id_solicitacao_tarefa' => $id_solicitacao_tarefa,
                'id_funcionario' => $id_funcionario
            );

            $this->model->insert_solicitacao_tarefa_func($funcionario);
        } 

        return true;
    }

    public function finalizar_implementacao($p){
        $s = $this->model->finalizar_implementacao($p['id_solicitacao']);
        if($s === 'solicitacao_sem_acoes'){
            $swal['titulo'] = 'Erro!';
            $swal['mensagem'] = 'Não foi possível finalizar a implementação! Solicitação não possui tarefas!';
            $swal['tipo'] = 'error';
            $this->session->set_flashdata('mensagem_swal', $swal);  

            return 'solicitacao_sem_acoes';
        } else {$log = array(
                'id_solicitacao' => $p['id_solicitacao'],
                'log' => 'Solicitação implementada pelo funcionário '.$this->session->userdata['tarefas']['nm_funcionario'].'.'
            );

            $this->model->insert_solicitacao_log($log);
            return true;
        }
    }

    public function aprovacao(){
        $p = $this->input->post();;

        if($p['acao'] == 'aprovar'){
            $status = $this->model->aprovar($p['id_solicitacao']);
        } else if($p['acao'] == 'reprovar') {
            $status = $this->reprovar($p);
        }
        if($status === false){
            $swal['titulo'] = 'Erro!';
            $swal['mensagem'] = 'Não foi possível executar a operação!';
            $swal['tipo'] = 'error';
            $this->session->set_flashdata('mensagem_swal', $swal);  
            redirect(base_url('minhas_tarefas'));
        }

        $swal['titulo'] = 'Sucesso!';
        $swal['mensagem'] = 'Ação realizada com sucesso!';
        $swal['tipo'] = 'success';
        $this->session->set_flashdata('mensagem_swal', $swal); 
        redirect(base_url('minhas_tarefas'));
    }

    public function reprovar($p){
        $this->model->reprovar($p['id_solicitacao']);

        $log = array(
            'id_solicitacao' => $p['id_solicitacao'],
            'log' => 'Solicitação reprovada pelo funcionário '.$this->session->userdata['tarefas']['nm_funcionario'].'. Motivo => "'.$p['motivo'].'".'
        );

        return $this->model->insert_solicitacao_log($log);
    }

    public function get_vw_tarefas_card(){
        $data['tarefas'] = $this->model->get_tarefas();
        echo $this->load->view('tarefas_card', $data, FALSE);
    }

    public function assumir_tarefa($idTarefa){
        $status = $this->model->assumir_tarefa($idTarefa);
        if($status === false){
            $swal['titulo'] = 'Erro!';
            $swal['mensagem'] = 'Não foi possível executar a operação!';
            $swal['tipo'] = 'error';
            $this->session->set_flashdata('mensagem_swal', $swal);  
            redirect(base_url('minhas_tarefas'));
        }
        $log = array(
            'id_solicitacao_tarefa' => $idTarefa,
            'log' => 'Tarefa assumida pelo funcionário '.$this->session->userdata['tarefas']['nm_funcionario'].'.'
        );
        $swal['titulo'] = 'Sucesso!';
        $swal['mensagem'] = 'Ação realizada com sucesso!';
        $swal['tipo'] = 'success';
        $this->session->set_flashdata('mensagem_swal', $swal); 

        $this->model->insert_solicitacao_tarefa_log($log);
        redirect(base_url('minhas_tarefas'));
    }

    public function finalizar(){
        $p = $this->input->post();
        // Cria o diretório padrão se ele não existe
        $dir = APPPATH . './uploads/tarefas/'.$p['id_solicitacao_tarefa'];
        if (!file_exists($dir)) {
            mkdir($dir, 0777, TRUE);
        }

        $qtd_arquivos = count($_FILES['arquivos']['name']);
        for($i = 0; $i < $qtd_arquivos; $i++){
            $_FILES['file']['name']     = $_FILES['arquivos']['name'][$i];
            $_FILES['file']['type']     = $_FILES['arquivos']['type'][$i];
            $_FILES['file']['tmp_name'] = $_FILES['arquivos']['tmp_name'][$i];
            $_FILES['file']['error']    = $_FILES['arquivos']['error'][$i];
            $_FILES['file']['size']     = $_FILES['arquivos']['size'][$i];
            
            // File upload configuration
            $config['upload_path']      = $dir;
            $config['allowed_types']    = '*';     
            $config['max_size']         = '10000';
            $config['file_name']        = $_FILES['file']['name'];
            
            // Load and initialize upload library
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            // Upload file to server
            if ($this->upload->do_upload('file')) {
                // Uploaded file data
                $upload_data = $this->upload->data();
                $solicitacao_tarefa_arquivo = array(
                    'ds_arquivo' => $upload_data['file_name'], 
                    'id_solicitacao_tarefa' => $p['id_solicitacao_tarefa']
                );
                $this->model->insert_solicitacao_tarefa_anexo($solicitacao_tarefa_arquivo);
            }
        }
        $status = $this->model->finalizar($p);   
        if($status === false){
            $swal['titulo'] = 'Erro!';
            $swal['mensagem'] = 'Não foi possível executar a operação!';
            $swal['tipo'] = 'error';
            $this->session->set_flashdata('mensagem_swal', $swal);  
            redirect(base_url('minhas_tarefas'));
        }   

        $swal['titulo'] = 'Sucesso!';
        $swal['mensagem'] = 'Ação realizada com sucesso!';
        $swal['tipo'] = 'success';
        $this->session->set_flashdata('mensagem_swal', $swal); 
        redirect(base_url('minhas_tarefas'));     
    }

    public function get_vw_modal_tarefa($idTarefa){
        $data['tarefa'] = $this->model->get_detalhes_tarefa($idTarefa);
        echo $this->load->view('modal_tarefa', $data, FALSE);
    }
}

