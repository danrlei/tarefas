<?php
if (!defined('BASEPATH')) {exit('No direct script access allowed');} 

class Setores extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->data['rodape'] = [];
        $this->data['rodape']['js'][] = modulo_js('setores.js');

        $this->load->model('grupos/grupos_model','grupos');
        $this->load->model('funcionarios/funcionarios_model','funcionarios');
    }

	public function index(){
        $this->data['interna'] = [];
        $this->data['carregar']['interna'] = 'setores';
        $this->data['interna']['grupos'] = $this->grupos->get_all();
        $this->data['interna']['funcionarios'] = $this->funcionarios->get_all();
        $this->data['interna']['dados'] = $this->model->get_all();
        $this->templates->padrao($this->data);
	}

    public function save(){
        $p = $this->input->post();
        $setor['ds_setor'] = $p['ds_setor'];
        $tipos_tarefas = $this->model->tratar_dados_tipo_tarefa($p);

        if(isset($p['id_setor']) && $p['id_setor'] != ""){
            $id_setor = $p['id_setor'];
            $status = $this->model->update($setor, $id_setor);
        } else {
            $status = $this->model->insert($setor);
            $id_setor = $this->db->insert_id();
        }
        if($status === false){
            $swal['titulo'] = 'Erro!';
            $swal['mensagem'] = 'Não foi possível executar a operação!';
            $swal['tipo'] = 'error';
            $this->session->set_flashdata('mensagem_swal', $swal);  
            redirect(base_url('setores'));
        }

        foreach($tipos_tarefas as $i => $tt){
            $tipo_tarefa = array(
                'id_setor'       => $id_setor,
                'ds_tipo_tarefa' => $tt['ds_tipo_tarefa']
            );

            if($tt['operacao_tipo_tarefa'] == 'I'){
                $status = $this->model->insert_tipo_tarefa($tipo_tarefa);
                $id_tipo_tarefa = $this->db->insert_id();
            } else if($tt['operacao_tipo_tarefa'] == 'U'){
                $id_tipo_tarefa = $tt['id_tipo_tarefa'];
                $status = $this->model->update_tipo_tarefa($tipo_tarefa, $id_tipo_tarefa);
                $this->model->delete_tipo_tarefa_grupo($id_tipo_tarefa);
                $this->model->delete_tipo_tarefa_func($id_tipo_tarefa);
            } else if($tt['operacao_tipo_tarefa'] == 'D'){
                $id_tipo_tarefa = $tt['id_tipo_tarefa'];
                $status = $this->model->delete_tipo_tarefa($id_tipo_tarefa);
            }

            if($status === false){
                $swal['titulo'] = 'Erro!';
                $swal['mensagem'] = 'Não foi possível executar a operação!';
                $swal['tipo'] = 'error';
                $this->session->set_flashdata('mensagem_swal', $swal);  
                redirect(base_url('setores'));
            }

            if($tt['operacao_tipo_tarefa'] != 'D'){
                // grupos
                if(isset($tt['tipo_tarefa_grupo'])){
                    foreach($tt['tipo_tarefa_grupo'] as $i2 => $g){
                        $tipo_tarefa_grupo = array(
                            'id_tipo_tarefa' => $id_tipo_tarefa,
                            'id_grupo'       => $g
                        );
                        $this->model->insert_tipo_tarefa_grupo($tipo_tarefa_grupo);
                    }
                }

                // funcionarios
                if(isset($tt['tipo_tarefa_func'])){
                    foreach($tt['tipo_tarefa_func'] as $i2 => $f){
                        $tipo_tarefa_func = array(
                            'id_tipo_tarefa' => $id_tipo_tarefa,
                            'id_funcionario' => $f
                        );
                        $this->model->insert_tipo_tarefa_func($tipo_tarefa_func);
                    }
                }

                // aprovacoes
                if(isset($tt['aprovacao'])){
                    foreach($tt['aprovacao'] as $i2 => $a){
                        $aprovacao = array(
                            'id_tipo_tarefa' => $id_tipo_tarefa,
                            'ds_aprovacao' => $a['ds_aprovacao']
                        ); 
                        if($a['operacao_aprovacao'] == 'I'){
                            $status = $this->model->insert_aprovacao($aprovacao);
                            $id_aprovacao = $this->db->insert_id();
                        } else if($a['operacao_aprovacao'] == 'U'){
                            $id_aprovacao = $a['id_aprovacao'];    
                            $status = $this->model->update_aprovacao($aprovacao, $id_aprovacao);  
                            $this->model->delete_aprovacao_grupo($id_aprovacao);
                            $this->model->delete_aprovacao_funcionario($id_aprovacao);                      
                        } else if($a['operacao_aprovacao'] == 'D'){
                            $id_aprovacao = $a['id_aprovacao'];    
                            $status = $this->model->delete_aprovacao($id_aprovacao);
                        }

                        if($status === false){
                            $swal['titulo'] = 'Erro!';
                            $swal['mensagem'] = 'Não foi possível executar a operação!';
                            $swal['tipo'] = 'error';
                            $this->session->set_flashdata('mensagem_swal', $swal);  
                            redirect(base_url('setores'));
                        }

                        if($a['operacao_aprovacao'] != 'D'){

                            // grupos
                            if(isset($a['grupos'])){
                                foreach($a['grupos'] as $i3 => $g3){
                                    $aprovacao_grupo = array(
                                        'id_aprovacao' => $id_aprovacao,
                                        'id_grupo'       => $g3
                                    );
                                    $this->model->insert_aprovacao_grupo($aprovacao_grupo);
                                }
                            }

                            // funcionarios
                            if(isset($a['funcionarios'])){
                                foreach($a['funcionarios'] as $i3 => $f3){
                                    $aprovacao_funcionario = array(
                                        'id_aprovacao' => $id_aprovacao,
                                        'id_funcionario' => $f3
                                    );
                                    $this->model->insert_aprovacao_funcionario($aprovacao_funcionario);
                                }
                            }

                        }
                    }
                }
            }
        }
        $swal['titulo'] = 'Sucesso!';
        $swal['mensagem'] = 'Ação realizada com sucesso!';
        $swal['tipo'] = 'success';
        $this->session->set_flashdata('mensagem_swal', $swal); 
        redirect(base_url('setores'));
    }

    public function get_by_id($idSetor){
        echo json_encode($this->model->get_by_id($idSetor));
    }

    public function delete($idGrupo){
        $status = $this->model->delete($idGrupo);
        if($status === false){
            $swal['titulo'] = 'Erro!';
            $swal['mensagem'] = 'Não foi possível executar a operação!';
            $swal['tipo'] = 'error';
            $this->session->set_flashdata('mensagem_swal', $swal);  
            redirect(base_url('setores'));
        }
        $swal['titulo'] = 'Sucesso!';
        $swal['mensagem'] = 'Ação realizada com sucesso!';
        $swal['tipo'] = 'success';
        redirect(base_url('setores'));
    }

    public function get_vw_tipo_tarefa(){
        $p = $this->input->post();
        if($p['id_tipo_tarefa'] !== 'false'){
            $data['tipo_tarefa'] = $this->model->get_tipo_tarefa_by_id($p['id_tipo_tarefa']);
        } else {
            $data['tipo_tarefa'][0]['tipo_tarefa_func'] = [];
            $data['tipo_tarefa'][0]['tipo_tarefa_grupo'] = [];
        }
        $data['seq_tipo_tarefa'] = $p['seq_tipo_tarefa'];
        $data['grupos'] = $this->grupos->get_all();
        $data['funcionarios'] = $this->funcionarios->get_all();
        echo $this->load->view('tipo_tarefa', $data, FALSE);
    }

    public function get_tipo_tarefa_by_setor($idSetor){
        echo json_encode($this->model->get_tipo_tarefa_by_setor($idSetor));
    }
}
