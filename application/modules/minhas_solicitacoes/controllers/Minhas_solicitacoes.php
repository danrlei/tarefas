<?php
if (!defined('BASEPATH')) {exit('No direct script access allowed');} 

class Minhas_solicitacoes extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->data['rodape'] = [];
        $this->data['rodape']['js'][] = modulo_js('minhas_solicitacoes.js');

        $this->load->model('setores/setores_model','setores');
    }

	public function index(){
        $this->data['interna'] = [];
        $this->data['carregar']['interna'] = 'minhas_solicitacoes';
        //$this->data['interna']['dados'] = $this->model->get_all();
        $this->data['interna']['setores'] = $this->setores->get_all();
        $this->templates->padrao($this->data);
	}

    public function save(){
        $solicitacao = $this->input->post();
        $solicitacao['id_status'] = 1;
        $solicitacao['id_solicitador'] = $this->session->userdata['tarefas']['id_funcionario'];

        $status = $this->model->insert($solicitacao);
        if($status === false){
            $swal['titulo'] = 'Erro!';
            $swal['mensagem'] = 'Não foi possível executar a operação!';
            $swal['tipo'] = 'error';
            $this->session->set_flashdata('mensagem_swal', $swal);  
            redirect(base_url('minhas_solicitacoes'));
        }

        $swal['titulo'] = 'Sucesso!';
        $swal['mensagem'] = 'Ação realizada com sucesso!';
        $swal['tipo'] = 'success';
        $this->session->set_flashdata('mensagem_swal', $swal); 
        redirect(base_url('minhas_solicitacoes'));
    }

    public function get_vw_solicitacao_card($tela){
        $data['solicitacoes'] = $this->model->get_solicitacoes($tela);
        $data['tela'] = $tela;
        echo $this->load->view('solicitacao_card', $data, FALSE);
    }
}
