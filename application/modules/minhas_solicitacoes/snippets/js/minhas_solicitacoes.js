$(document).ready(function() {
    getSolicitacoes();
});

$(document).on('change', '#setores', function(){
	setor = $(this).val();
	$.ajax({
        url: base_url + "setores/get_tipo_tarefa_by_setor/"+setor,
        type: 'get',
        dataType: 'json',
        success:function (data) {
        	option = "";
        	data.forEach(function(tp){
        		option += "<option value="+tp.id_tipo_tarefa+">"+tp.ds_tipo_tarefa+"</option>";
        	});
        	$('#tipo_tarefa').html(option);
        }
    }); 
})

function getSolicitacoes(){
    $.ajax({
        url: base_url + "minhas_solicitacoes/get_vw_solicitacao_card/minhas_solicitacoes",
        type: 'get',
        dataType: 'html',
        success:function (data) {
            $('#solicitacoes').html(data);
        }
    });  
}

function getDetalhesTarefa(idTarefa){
    $.ajax({
        url: base_url + "minhas_tarefas/get_vw_modal_tarefa/"+idTarefa,
        type: 'get',
        dataType: 'html',
        success:function (data) {
            $('#modal-detalhes-tarefa .modal-body').html(data);
        }
    });  
}