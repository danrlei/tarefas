<?php foreach($solicitacoes as $i => $s){ ?>

<a class="card-header d-flex align-items-center collapsed" data-toggle="collapse" href="#solicitacao-<?php echo $s['id_solicitacao']; ?>" aria-expanded="false">
    <div class="card-title " style="font-size: 1.2rem; width: 100%;">
    	<div class="row">
    		<div class="col-1"><?php echo $s['id_solicitacao']; ?></div>
    		<div class="col-4"><?php echo $s['titulo']; ?></div>
    		<div class="col-3"><?php echo $s['ds_status']; ?></div>
            <div class="col-1" style="text-align: center;"><?php echo $s['percentual_conclusao']; ?>%</div>
            <div class="col-1"><?php echo $s['dt_prazo']; ?></div>
    		<div class="col-2">
            <?php if($tela == 'minhas_tarefas'){ ?>
                <?php if($s['acao'] == 'assumir'){ ?>
                    <button style="margin-top: -9px;" type="button" class="btn btn-shadow btn-novo"onClick="assumir(<?php echo $s['id_solicitacao']; ?>);"><?php echo $s['acao_texto']; ?></button>
                <?php } else {?>
                    <button style="margin-top: -9px;" type="button" class="btn btn-shadow btn-novo" data-toggle="modal" data-target="#modal-<?php echo $s['acao']; ?>" onClick="<?php echo $s['acao']; ?>(<?php echo $s['id_solicitacao']; ?>);"><?php echo $s['acao_texto']; ?></button>
                <?php } ?>
            <?php } ?>
            </div>
    	</div>
    </div>
</a>
<div id="solicitacao-<?php echo $s['id_solicitacao']; ?>" class="card-body pt-0 collapse row" data-parent="#accordion" style="font-size: 1.2rem;">
    <div class="col-3 m--margin-bottom-10">
    	<b>Setor</b><br/>
        <?php echo $s['ds_setor']; ?>
    </div>
    <div class="col-3 m--margin-bottom-10">
    	<b>Tipo de Tarefa</b><br/>
        <?php echo $s['ds_tipo_tarefa']; ?>
    </div>
    <div class="col-3 m--margin-bottom-10">
    	<b>Criação</b><br/>
        <?php echo $s['dt_criacao']; ?>
    </div>
    <div class="col-3 m--margin-bottom-10">
    	<b>Solicitador</b><br/>
        <?php echo $s['nm_funcionario']; ?>
    </div>
    <div class="col-12 m--margin-bottom-30">
    	<b>Descrição</b><br/>
        <?php echo $s['ds_solicitacao']; ?>
    </div>
    <div class="col-12 m--margin-bottom-10">
        <h3>Tarefas </h3>
    </div>
    <div class="col-12 m--margin-bottom-30">
        <div class="table-responsive">
            <table class="table mb-0">
                <thead>
                    <tr>
                        <th style="width: 5%">ID</th>
                        <th>Descrição</th>
                        <th>Criação</th>
                        <th>Funcionários</th>
                        <th>Grupos</th>
                        <th>Pendência</th>
                        <th>Status</th>
                        <th>Prazo</th>
                        <th style="width: 5%"></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($s['tarefas'] as $i2 => $l){ ?>
                    <tr>
                        <td><?php echo $l['id_solicitacao_tarefa'] ?></td>
                        <td><?php echo $l['ds_solicitacao_tarefa'] ?></td>
                        <td><?php echo $l['criacao'] ?></td>
                        <td><?php echo $l['funcionarios'] ?></td>
                        <td><?php echo $l['grupos'] ?></td>
                        <td><?php echo $l['id_tarefa_pendencia'] ?></td>
                        <td><?php echo $l['ds_status'] ?></td>
                        <td><?php echo $l['dt_prazo'] ?></td>
                        <td style="width: 5%; text-align: center;" onclick="getDetalhesTarefa(<?php echo $l['id_solicitacao_tarefa'] ?>)" class="td-actions">
                            <a href="#" data-toggle="modal" data-target="#modal-detalhes-tarefa">
                                <i class="la la-search"></i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-12 m--margin-bottom-10">
        <h3>Aprovações </h3>
    </div>
    <div class="col-12 m--margin-bottom-30">
        <div class="table-responsive">
            <table class="table mb-0">
                <thead>
                    <tr>
                        <th style="width: 55%">Aprovação</th>
                        <th style="width: 20%">Data da Aprovação</th>
                        <th style="width: 25%">Aprovador</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($s['aprovacoes'] as $i2 => $l){ ?>
                    <tr>
                        <td><?php echo $l['ds_aprovacao'] ?></td>
                        <td><?php echo $l['dt_aprovacao'] ?></td>
                        <td><?php echo $l['nm_funcionario'] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-12 m--margin-bottom-10">
        <h3>Histórico </h3>
    </div>
    <div class="col-12 m--margin-bottom-10">
        <div class="table-responsive">
            <table class="table mb-0">
                <thead>
                    <tr>
                        <th style="width: 70%">Descrição</th>
                        <th style="width: 20%">Data</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($s['logs'] as $i2 => $l){ ?>
                    <tr>
                        <td><?php echo $l['log'] ?></td>
                        <td><?php echo $l['criacao'] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php } ?>

<div id="modal-detalhes-tarefa" class="modal fade">
    <div class="modal-dialog modal-dialog-centered modal-lg" style="max-width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detalhes da Tarefa</h4>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">close</span>
                </button>
            </div>
            <form class="form-horizontal align-items-center" id="form-solicitacao" method="post" action="<?php echo base_url('minhas_solicitacoes/save')?>">
                <div class="modal-body">



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-shadow btn-cancelar" data-dismiss="modal">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>