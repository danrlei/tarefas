
                <div class="widget widget-21 has-shadow">
                    <div class="widget-header bordered no-actions d-flex align-items-center ">
                        <h2>Minhas Solicitações</h2>
                    </div>
                    <div class="widget-body m--padding-top-20  m--padding-bottom-0">
                        <div class="form-group">
                        	<button type="button" class="btn btn-shadow btn-novo mr-1 mb-2 m--margin-right-30 btn-incluir" data-toggle="modal" data-target="#modal-setor" onClick="resetar_form('form-solicitacao');">Solicitar Tarefa</button>
                        </div>
						<div class="col-lg-12  col-lg-mobile  m--padding-bottom-15 m--padding-top-50">
                            <div id="accordion" class="accordion">
                                <!-- Begin Widget -->
                                <div class="row titulo-card">
                                    <div class="col-1">ID</div>
                                    <div class="col-4">Título</div>
                                    <div class="col-3">Status</div>
                                    <div class="col-1">Concluído</div>
                                    <div class="col-1">Prazo</div>
                                    <div class="col-2"></div>
                                </div>
                                <div class="widget has-shadow" id="solicitacoes">

                                </div>
                                <!-- End Widget -->
                            </div>
						</div>
					</div>
				</div>


<div id="modal-setor" class="modal fade">
    <div class="modal-dialog modal-dialog-centered modal-lg" style="max-width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Solicitação</h4>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">close</span>
                </button>
            </div>
            <form class="form-horizontal align-items-center" id="form-solicitacao" method="post" action="<?php echo base_url('minhas_solicitacoes/save')?>">
                <div class="modal-body">
                    <div class="form-group row d-flex align-items-center ">
                        <div class="col-md-12 m--margin-bottom-10">
                            <label class="form-control-label">Título</label>
                            <input type="text" class="form-control" name="titulo" maxlength="1000" required>
                        </div>                        
                        <div class="col-md-6 m--margin-bottom-10">
                            <label class="form-control-label">Setor</label>
                            <select id="setores" required class="form-control">
                                <?php foreach($setores as $s){ ?>
                                    <option value="<?php echo $s['id_setor'] ?>"><?php echo $s['ds_setor'] ?></option>
                                <?php } ?>
                            </select>
                        </div>                       
                        <div class="col-md-6 m--margin-bottom-10">
                            <label class="form-control-label">Tipo de tarefa</label>
                            <select id="tipo_tarefa" required name="id_tipo_tarefa" class="form-control">
                            </select>
                        </div>
                        <div class="col-md-12 m--margin-bottom-10">
                            <label class="form-control-label">Descrição</label>
                            <textarea class="form-control" rows="5" required name="ds_solicitacao"></textarea>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-shadow btn-cancelar" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>