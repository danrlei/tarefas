<?php
class Minhas_solicitacoes_model extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function get_solicitacoes($tela){
		$this->db->select("
			case 
				when (s.id_executante is null and sta.id_status <> 2) then 'Assumir'
				when sta.id_status = 1 then 'Implementar'
				when sta.id_status = 2 then 'Aprovação'
			end as acao_texto,
			case 
				when (s.id_executante is null and sta.id_status <> 2)  then 'assumir'
				when sta.id_status = 1 then 'implementar'
				when sta.id_status = 2 then 'aprovar'
			end as acao,
			s.id_solicitacao, 
			s.titulo, 
			regexp_replace(s.ds_solicitacao, E'\r\n|\n|\r', '<br />', 'g') ds_solicitacao, 
			to_char(s.dt_criacao, 'DD/MM/YYYY HH24:MI') dt_criacao,
			to_char(s.dt_prazo, 'DD/MM/YYYY') dt_prazo,
			st.id_setor,
			st.ds_setor,
			tt.id_tipo_tarefa,
			tt.ds_tipo_tarefa,
			f.id_funcionario,
			f.nm_funcionario,
			sta.id_status,
			sta.ds_status,
			case when (select count(1) from solicitacao_tarefa st2 where st2.id_solicitacao = s.id_solicitacao) = 0 
				then 0.00
				else 
					round((
						cast((select count(1) from solicitacao_tarefa st2 where id_status = 4 and st2.id_solicitacao = s.id_solicitacao) as decimal) / 
						cast((select count(1) from solicitacao_tarefa st2 where st2.id_solicitacao = s.id_solicitacao) as decimal)

					) * 100, 2)
			end as percentual_conclusao
		");
		$this->db->from('solicitacao s');
		$this->db->join('tipo_tarefa tt', 's.id_tipo_tarefa = tt.id_tipo_tarefa');
		$this->db->join('setor st', 'st.id_setor = tt.id_setor');
		$this->db->join('funcionario f', 'f.id_funcionario = s.id_solicitador');
		$this->db->join('status sta', 'sta.id_status = s.id_status');

		if($tela == 'minhas_solicitacoes'){
			$this->db->where('s.id_solicitador', $this->session->userdata['tarefas']['id_funcionario']);
		} else if($tela == 'minhas_tarefas'){
			// Implementação
			$this->db->where('(
				sta.id_status = 1 and 
				'.$this->session->userdata['tarefas']['id_funcionario'].' in (
					select fg2.id_funcionario
					from tipo_tarefa tt2
					join tipo_tarefa_grupo ttg2 on tt2.id_tipo_tarefa = ttg2.id_tipo_tarefa
					join grupo g2 on ttg2.id_grupo = g2.id_grupo
					join funcionario_grupo fg2 on g2.id_grupo = fg2.id_grupo
					where tt2.id_tipo_tarefa = tt.id_tipo_tarefa
					union
					select ttf3.id_funcionario
					from tipo_tarefa tt3
					join tipo_tarefa_func ttf3 on tt3.id_tipo_tarefa = ttf3.id_tipo_tarefa
					where tt3.id_tipo_tarefa = tt.id_tipo_tarefa
				) OR 
				sta.id_status = 2 and 
				'.$this->session->userdata['tarefas']['id_funcionario'].' in (
					select fg2.id_funcionario
					from tipo_tarefa tt2
					join aprovacao a2 on tt2.id_tipo_tarefa = a2.id_tipo_tarefa
					left join solicitacao_aprovacao sa2 on sa2.id_aprovacao = a2.id_aprovacao and sa2.id_solicitacao = s.id_solicitacao
					join aprovacao_grupo ag2 on a2.id_aprovacao = ag2.id_aprovacao
					join grupo g2 on g2.id_grupo = ag2.id_grupo
					join funcionario_grupo fg2 on g2.id_grupo = fg2.id_grupo
					where tt2.id_tipo_tarefa = tt.id_tipo_tarefa
					and sa2.id_solicitacao_aprovacao is null
					union
					select  af3.id_funcionario
					from tipo_tarefa tt3
					join aprovacao a3 on tt3.id_tipo_tarefa = a3.id_tipo_tarefa
					left join solicitacao_aprovacao sa3 on sa3.id_aprovacao = a3.id_aprovacao and sa3.id_solicitacao = s.id_solicitacao
					join aprovacao_funcionario af3 on a3.id_aprovacao = af3.id_aprovacao
					where tt3.id_tipo_tarefa = tt.id_tipo_tarefa
					and sa3.id_solicitacao_aprovacao is null
				)
			)');
			$this->db->where('(s.id_executante is null or s.id_executante = '.$this->session->userdata['tarefas']['id_funcionario'].')');
		}
		$this->db->order_by('s.dt_criacao desc');
		$retorno = $this->db->get()->result_array();
		foreach($retorno as $i => $r){
			$this->db->select('id_solicitacao_log, log, to_char(dt_criacao, \'DD/MM/YYYY HH24:MI:SS\') as criacao');
			$this->db->from('solicitacao_log');
			$this->db->where('id_solicitacao', $r['id_solicitacao']);
			$this->db->order_by('dt_criacao desc');
			$retorno[$i]['logs'] = $this->db->get()->result_array();

			$this->db->select("distinct st.id_solicitacao_tarefa, ds_solicitacao_tarefa, to_char(st.dt_criacao, 'DD/MM/YYYY') as criacao, STRING_AGG(distinct f.nm_funcionario, ', ') as funcionarios, STRING_AGG(distinct g.ds_grupo, ', ') as grupos, id_tarefa_pendencia, sta.ds_status, , to_char(st.dt_prazo, 'DD/MM/YYYY') as dt_prazo", false);
			$this->db->from('solicitacao_tarefa st');
			$this->db->join('solicitacao_tarefa_func stf','st.id_solicitacao_tarefa = stf.id_solicitacao_tarefa','left');
			$this->db->join('solicitacao_tarefa_grupo stg','st.id_solicitacao_tarefa = stg.id_solicitacao_tarefa','left');
			$this->db->join('funcionario f','stf.id_funcionario = f.id_funcionario','left');
			$this->db->join('grupo g','stg.id_grupo = g.id_grupo','left');
			$this->db->join('status sta', 'st.id_status = sta.id_status');
			$this->db->where('id_solicitacao', $r['id_solicitacao']);
			$this->db->group_by('st.id_solicitacao_tarefa, sta.ds_status');
			$this->db->order_by('st.id_solicitacao_tarefa');

			$retorno[$i]['tarefas'] = $this->db->get()->result_array();

			$this->db->select('a.ds_aprovacao, f.nm_funcionario, to_char(sa.dt_criacao, \'DD/MM/YYYY HH24:MI\') as dt_aprovacao');
			$this->db->from('solicitacao s');
			$this->db->join('tipo_tarefa tt', 's.id_tipo_tarefa = tt.id_tipo_tarefa');
			$this->db->join('aprovacao a', 'tt.id_tipo_tarefa = a.id_tipo_tarefa');
			$this->db->join('solicitacao_aprovacao sa', 's.id_solicitacao = sa.id_solicitacao AND sa.id_aprovacao = a.id_aprovacao', 'left');
			$this->db->join('funcionario f', 'sa.id_aprovador = f.id_funcionario', 'left');
			$this->db->where('s.id_solicitacao', $r['id_solicitacao']);
			$this->db->order_by('sa.dt_criacao');

			$retorno[$i]['aprovacoes'] = $this->db->get()->result_array();
		}
		return $retorno;
	}

	public function insert($dados){
		return $this->db->insert('solicitacao',$dados);
	}

}